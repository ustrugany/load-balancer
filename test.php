<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */

require 'vendor/autoload.php';

use LoadBalancer\Host\Host;

$host = new Host(0.75);