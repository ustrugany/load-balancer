<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace LoadBalancer;

use LoadBalancer\Request\Request;

interface LoadBalancerInterface
{
    /**
     * @param Request $request
     */
    public function handleRequest(Request $request);
}
