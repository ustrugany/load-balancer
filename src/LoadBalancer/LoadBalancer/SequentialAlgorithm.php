<?php

namespace LoadBalancer\LoadBalancer;

use LoadBalancer\Host\HostInterface;
use LoadBalancer\Request\Request;

class SequentialAlgorithm implements BalancingAlgorithmInterface
{
    /**
     * @param  Request $request
     * @return mixed
     */
    public function balance(Request $request, array $hosts)
    {
        /** @var HostInterface $host */
        foreach ($hosts as $host) {
            $host->handleRequest($request);
        }
    }
}
