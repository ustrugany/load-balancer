<?php

namespace LoadBalancer\LoadBalancer;

use LoadBalancer\Host\HostInterface;
use LoadBalancer\Request\Request;

class FirstWithLowestLoadAlgorithm implements BalancingAlgorithmInterface
{
    const LOAD_THRESHOLD = 0.75;

    /**
     * @var HostInterface
     */
    private $hostBelowThreshold;

    /**
     * {@inheritdoc}
     */
    public function balance(Request $request, array $hosts)
    {
        $this->hostBelowThreshold = null;
        $sorted = $this->sortByLoad($hosts);

        $host = current($sorted);
        $host->handleRequest($request);
    }

    /**
     * @param  array                                    $hosts
     * @return array|\LoadBalancer\Host\HostInterface[]
     */
    private function sortByLoad(array $hosts)
    {
        /** @var HostInterface[] $hosts */
        if ($this->hostBelowThreshold) {
            return $hosts;
        }

        if (1 >= count($hosts)) {
            return $hosts;
        }

        if ($hosts[0]->getLoad() < self::LOAD_THRESHOLD) {
            $this->hostBelowThreshold = $hosts[0];

            return $hosts;
        }

        $element = array_shift($hosts);
        $left = $right = [];

        foreach ($hosts as $host) {
            if ($host->getLoad() < $element->getLoad()) {
                $left[] = $host;
            } else {
                $right[] = $host;
            }
        }

        return array_merge($this->sortByLoad($left), [$element], $this->sortByLoad($right));
    }
}
