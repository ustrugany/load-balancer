<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace LoadBalancer\LoadBalancer;

interface BalancingAlgorithmFactoryInterface
{
    /**
     * @param  string                                           $algorithmType
     * @return FirstWithLowestLoadAlgorithm|SequentialAlgorithm
     * @throws \InvalidArgumentException
     */
    public function create($algorithmType);
}
