<?php

namespace LoadBalancer\LoadBalancer;

use LoadBalancer\Request\Request;

interface BalancingAlgorithmInterface
{
    /**
     * @param  Request $request
     * @param  array   $hosts
     * @return null
     */
    public function balance(Request $request, array $hosts);
}
