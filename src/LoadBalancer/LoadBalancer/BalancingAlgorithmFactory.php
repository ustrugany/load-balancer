<?php

namespace LoadBalancer\LoadBalancer;

class BalancingAlgorithmFactory implements BalancingAlgorithmFactoryInterface
{
    const SEQUENTIAL_ALGORITHM = 'sequential';
    const LOWEST_LOAD_FIRST_ALGORITHM = 'lowest_load_first';

    /**
     * {@inheritdoc}
     */
    public function create($algorithmType)
    {
        switch ($algorithmType) {
            case self::SEQUENTIAL_ALGORITHM:
                return new SequentialAlgorithm();
            case self::LOWEST_LOAD_FIRST_ALGORITHM:
                return new FirstWithLowestLoadAlgorithm();
            default:
                throw new \InvalidArgumentException();
        }
    }
}
