<?php

namespace LoadBalancer\Host;

use LoadBalancer\Request\RequestInterface;

class Host implements HostInterface
{
    /**
     * @var int
     */
    private $load;

    /**
     * @param int $load
     */
    public function __construct($load)
    {
        if (!is_numeric($load)) {
            throw new \InvalidArgumentException();
        }

        if ($load < 0) {
            throw new \InvalidArgumentException();
        }

        $this->load = (float) $load;
    }

    /**
     * @return int
     */
    public function getLoad()
    {
        return $this->load;
    }

    /**
     * @param RequestInterface $request
     */
    public function handleRequest(RequestInterface $request)
    {
        /**
         * request processing
         */
    }
}
