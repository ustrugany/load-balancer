<?php
/**
 * @author Piotr Strugacz <piotr.strugacz@xsolve.pl>
 */
namespace LoadBalancer\Host;

use LoadBalancer\Request\RequestInterface;

interface HostInterface
{
    /**
     * @return int
     */
    public function getLoad();

    /**
     * @param RequestInterface $request
     */
    public function handleRequest(RequestInterface $request);
}
