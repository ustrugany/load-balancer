<?php

namespace LoadBalancer;

use LoadBalancer\Host\HostInterface;
use LoadBalancer\LoadBalancer\BalancingAlgorithmFactory;
use LoadBalancer\Request\Request;

class LoadBalancer implements LoadBalancerInterface
{
    /**
     * @var array
     */
    private $hosts;

    /**
     * @var string
     */
    private $algorithm;

    /**
     * @param array $hosts
     * @param $algorithmType
     */
    public function __construct(array $hosts, $algorithmType)
    {
        if ([] == $hosts) {
            throw new \InvalidArgumentException();
        }

        $this->algorithm = (new BalancingAlgorithmFactory())->create($algorithmType);

        if (!$this->isHostsArrayValid($hosts)) {
            throw new \InvalidArgumentException();
        }

        $this->hosts = $hosts;
    }

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request)
    {
        $this->algorithm->balance($request, $this->hosts);
    }

    /**
     * @param  array $hosts
     * @return bool
     */
    private function isHostsArrayValid(array $hosts)
    {
        /** @var HostInterface $host */
        foreach ($hosts as $host) {
            if (!$host instanceof HostInterface) {
                return false;
            }
        }

        return true;
    }
}
