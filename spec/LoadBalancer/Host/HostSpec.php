<?php

namespace spec\LoadBalancer\Host;

use LoadBalancer\Request\Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class HostSpec extends ObjectBehavior
{
    const LOAD_VALUE = 0.75;

    function let()
    {
        $this->beConstructedWith(self::LOAD_VALUE);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\Host\Host');
    }

    function it_validates_load_given()
    {
        $this->shouldThrow('\InvalidArgumentException')->during('__construct', ['value']);
        $this->shouldThrow('\InvalidArgumentException')->during('__construct', [-0.75]);
    }

    function it_returns_load_constructed_with()
    {
        $this->getLoad()->shouldBeFloat();
        $this->getLoad()->shouldEqual(self::LOAD_VALUE);
    }

    function it_handles_request(Request $request)
    {
        $this->handleRequest($request)->shouldReturn(null);
    }
}
