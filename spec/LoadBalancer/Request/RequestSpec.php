<?php

namespace spec\LoadBalancer\Request;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RequestSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\Request\Request');
    }
}
