<?php

namespace spec\LoadBalancer\LoadBalancer;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BalancingAlgorithmFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\LoadBalancer\BalancingAlgorithmFactory');
    }

    function it_creates_sequential_algorithm()
    {
        $this->create('sequential')->shouldReturnAnInstanceOf('LoadBalancer\LoadBalancer\SequentialAlgorithm');
    }

    function it_creates_first_with_lowest_load_algorithm()
    {
        $this->create('lowest_load_first')->shouldReturnAnInstanceOf('LoadBalancer\LoadBalancer\FirstWithLowestLoadAlgorithm');
    }

    function it_validates_invalid_algorithm_type()
    {
        $this->shouldThrow('\InvalidArgumentException')->during('create', ['invalid_algorithm']);
    }
}
