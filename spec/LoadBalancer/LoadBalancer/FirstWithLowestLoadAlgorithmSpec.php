<?php

namespace spec\LoadBalancer\LoadBalancer;

use LoadBalancer\Host\Host;
use LoadBalancer\Request\Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FirstWithLowestLoadAlgorithmSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\LoadBalancer\FirstWithLowestLoadAlgorithm');
    }

    function it_balances_request_to_first_host_with_lowest_load(
        Request $request,
        Host $host1,
        Host $host2,
        Host $host3,
        Host $host4
    ) {
        $host1->getLoad()->willReturn(0.78);
        $host1->handleRequest($request)->shouldNotBeCalled();
        $host2->getLoad()->willReturn(0.81);
        $host2->handleRequest($request)->shouldNotBeCalled();
        $host3->getLoad()->willReturn(0.85);
        $host3->handleRequest($request)->shouldNotBeCalled();
        $host4->getLoad()->willReturn(0.76);
        $host4->handleRequest($request)->shouldBeCalledTimes(1);

        $this->balance($request, [$host1, $host2, $host3, $host4])->shouldReturn(null);
    }

    function it_handles_request_to_first_host_with_load_below_threshold(
        Request $request,
        Host $host1,
        Host $host2,
        Host $host3,
        Host $host4,
        Host $host5,
        Host $host6
    ) {
        $host1->getLoad()->willReturn(0.91);
        $host1->handleRequest($request)->shouldNotBeCalled();

        $host2->getLoad()->willReturn(0.76);
        $host2->handleRequest($request)->shouldNotBeCalled();

        $host3->getLoad()->willReturn(0.86);
        $host3->handleRequest($request)->shouldNotBeCalled();

        $host4->getLoad()->willReturn(0.66);
        $host4->handleRequest($request)->shouldBeCalledTimes(1);

        $host5->getLoad()->willReturn(0.95);
        $host5->handleRequest($request)->shouldNotBeCalled();

        $host6->getLoad()->willReturn(0.71);
        $host6->handleRequest($request)->shouldNotBeCalled();

        $this->balance($request, [$host1, $host2, $host3, $host4, $host5, $host6])->shouldReturn(null);
    }



}
