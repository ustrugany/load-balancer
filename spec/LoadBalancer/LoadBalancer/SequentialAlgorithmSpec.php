<?php

namespace spec\LoadBalancer\LoadBalancer;

use LoadBalancer\Host\Host;
use LoadBalancer\Request\Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SequentialAlgorithmSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\LoadBalancer\SequentialAlgorithm');
    }

    function it_balances_request_sequentially(
        Request $request,
        Host $host1,
        Host $host2,
        Host $host3
    ) {
        $host1->getLoad()->willReturn(0.75);
        $host1->handleRequest($request)->shouldBeCalledTimes(1);
        $host2->getLoad()->willReturn(0.50);
        $host2->handleRequest($request)->shouldBeCalledTimes(1);
        $host3->getLoad()->willReturn(0.85);
        $host3->handleRequest($request)->shouldBeCalledTimes(1);

        $this->balance($request, [$host1, $host2, $host3])->shouldReturn(null);
    }
}
