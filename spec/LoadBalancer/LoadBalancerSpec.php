<?php

namespace spec\LoadBalancer;

use LoadBalancer\Host\Host;
use LoadBalancer\Request\Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LoadBalancerSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith([], 'sequential');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('LoadBalancer\LoadBalancer');
    }

    function it_validates_invalid_hosts_array()
    {
        $this->shouldThrow('\InvalidArgumentException')->during('__construct', [[], 'sequential']);
        $this->shouldThrow('\InvalidArgumentException')->during('__construct', [[new \stdClass()], 'sequential']);
    }

    function it_handles_request_sequentially(Request $request, Host $host1, Host $host2, Host $host3)
    {
        $host1->getLoad()->willReturn(0.75);
        $host1->handleRequest($request)->shouldBeCalledTimes(1);
        $host2->getLoad()->willReturn(0.50);
        $host2->handleRequest($request)->shouldBeCalledTimes(1);
        $host3->getLoad()->willReturn(0.85);
        $host3->handleRequest($request)->shouldBeCalledTimes(1);

        $this->beConstructedWith([$host1, $host2, $host3], 'sequential');
        $this->handleRequest($request)->shouldReturn(null);
    }

    function it_handles_request_to_first_host_with_lowest_load(
        Request $request,
        Host $host1,
        Host $host2,
        Host $host3,
        Host $host4
    ) {
        $host1->getLoad()->willReturn(0.78);
        $host1->handleRequest($request)->shouldNotBeCalled();
        $host2->getLoad()->willReturn(0.81);
        $host2->handleRequest($request)->shouldNotBeCalled();
        $host3->getLoad()->willReturn(0.85);
        $host3->handleRequest($request)->shouldNotBeCalled();
        $host4->getLoad()->willReturn(0.76);
        $host4->handleRequest($request)->shouldBeCalledTimes(1);

        $this->beConstructedWith([$host1, $host2, $host3, $host4], 'lowest_load_first');
        $this->handleRequest($request)->shouldReturn(null);
    }

    function it_handles_request_to_first_host_with_load_below_threshold(
        Request $request,
        Host $host1,
        Host $host2,
        Host $host3,
        Host $host4
    ) {
        $host1->getLoad()->willReturn(0.91);
        $host1->handleRequest($request)->shouldNotBeCalled();
        $host2->getLoad()->willReturn(0.75);
        $host2->handleRequest($request)->shouldNotBeCalled();
        $host3->getLoad()->willReturn(0.81);
        $host3->handleRequest($request)->shouldNotBeCalled();
        $host4->getLoad()->willReturn(0.74);
        $host4->handleRequest($request)->shouldBeCalledTimes(1);

        $this->beConstructedWith([$host1, $host2, $host3, $host4], 'lowest_load_first');
        $this->handleRequest($request)->shouldReturn(null);
    }

}
